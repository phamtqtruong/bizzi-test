Giải thích
---
Duyệt qua mảng, khởi tạo giá trị cho lần đầu tiên gặp giá trị mới, trừ dần khi giá trị duyệt qua không phải là giá trị đầu tiên gặp.

Hiệu suất
---
Với giải thuật trên, và n là số phần tử của mảng được cung cấp thì độ phức tạp của chương trình sẽ là O(n) vì tốn hầu hết thời gian cho thao tác duyệt mảng.
