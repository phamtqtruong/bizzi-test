const firstProblemResolve = (arr, repeat = 3) => {
  if (!arr || !Array.isArray(arr) || Array.length <= 0 || parseInt(repeat, 10) !== repeat) {
    throw new Error("Input chưa hợp lệ");
  }
  const history = [];
  let ret = 0;
  for (let i = 0; i < arr.length; i += 1) {
    if (!history[arr[i]]) {
      ret = arr[i] * (repeat - 1);
      history[arr[i]] = 1;
    } else {
      ret -= arr[i];
    }
  }
  return ret / (repeat - 1);
};

module.exports = { firstProblemResolve };
