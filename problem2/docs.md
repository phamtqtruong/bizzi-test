Giải thích
---
Với cây nhị phân thì phần tử cháu của một node sẽ là 4n + 3, 4n + 4, 4n + 5, 4n + 6. Việc cần làm là duyệt qua mảng được cho và kiểm tra xem giá trị vừa duyệt qua có phải là số chẵn hay không sau đó cộng dồn vào kết quả.

Hiệu suất
---
Với giải thuật trên, và n là số phần tử của cây nhị phân, d là độ sâu của cây thì độ phức tạp của chương trình sẽ là O(n/4) vì các node ở độ sâu bằng d - 1 và d không có cháu, nên ta chỉ cần duyệt qua n/4 phần tử của mảng.
