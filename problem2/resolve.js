
const secondProblemResolve = (arr) => {
  if (!arr || !Array.isArray(arr) || arr.length <= 0) {
    throw new Error("Input chưa hợp lệ");
  }
  const len = arr.length;
  maxLoopIndex = ((len + 1) / 4) - 1;
  let ret = 0;
  for (let i = 0; i <= maxLoopIndex; i += 1) {
    if (arr[i] % 2 === 0) {
      ret += (arr[4 * i + 3] || 0) + (arr[4 * i + 4] || 0) + (arr[4 * i + 5] || 0) + (arr[4 * i + 6] || 0);
    }
  }
  return ret;
};

module.exports = { secondProblemResolve };
