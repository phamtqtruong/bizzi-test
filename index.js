const { firstProblemResolve } = require("./problem1/resolve");
const { secondProblemResolve } = require("./problem2/resolve");

console.log("Problem 1 - Example 1 output:", firstProblemResolve([1, 1, 0, 1]));
console.log("Problem 1 - Example 2 output:", firstProblemResolve([3, 1, 3, 3, 5, 1, 1]));
console.log("Problem 2 - Example output:", secondProblemResolve([6, 7, 8, 2, 7, 1, 3, 9, null, 1, 4, null, null, null, 5]));
